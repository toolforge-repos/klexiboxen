var change_counter = 0;

$(document).ready(function(){
    $("#tbl_changelog #checkall").click(function () {
        if ($("#tbl_changelog #checkall").is(':checked')) {
            $("#tbl_changelog input[type=checkbox]").each(function () {
                $(this).prop("checked", true);
                $(this).closest('tr').addClass('table-success');
            });
            update_counter ($(".checkthis").length);
        } else {
            $("#tbl_changelog input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
                $(this).closest('tr').removeClass('table-success');
            });
            update_counter (0);
        }
    });

    $(".checkthis").click(function () {
        if ($(this).is(':checked')) {
            $(this).closest('tr').addClass('table-success');
            update_counter (change_counter + 1);
        } else {
            $(this).closest('tr').removeClass('table-success');
            update_counter (change_counter - 1);
        }
    });

    $('#tbl_changelog tr').click(function(event) {
        if (event.target.type !== 'checkbox') {
            $(':checkbox', this).trigger('click');
        }
  });
});

function update_counter(new_value) {
    change_counter = new_value;
    if (change_counter == 0) {
        $("#lbl_counter").text("Bitte erst Änderungen auswählen");
        $("#btn_transfer").prop("disabled", true);
    } else {
        $("#lbl_counter").text(change_counter + " Änderungen ausgewählt");
        $("#btn_transfer").prop("disabled", false);
    }
}
